    <!-- jQuery -->
    <script src="{{ asset('public/js/jquery-1.11.3.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{ asset('public/js/jquery.easing.min.js') }}"></script>
    
    <!-- Custom Javascript -->
    <script src="{{ asset('public/js/custom.js') }}"></script>